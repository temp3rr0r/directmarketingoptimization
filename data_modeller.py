import operator
from sklearn.ensemble import RandomForestClassifier
from sklearn import metrics, linear_model
from sklearn.neighbors import KNeighborsRegressor
from sklearn.metrics import confusion_matrix, mean_squared_error
from sklearn.metrics import roc_curve, auc
from sklearn.linear_model import LogisticRegression, LinearRegression
from sklearn.ensemble import RandomForestRegressor
from sklearn.model_selection import train_test_split
from sklearn.svm import SVC


class DataModeller:
    """
    Trains models, given target y labels and pandas data-sets. Stores model performance metrics.
    """

    def __init__(self):
        """
        Initialize public lists and dictionaries.
        """
        self.y_binary_classification_labels = ["Sale_MF", "Sale_CC", "Sale_CL"]
        self.y_regression_labels = ["Revenue_MF", "Revenue_CC", "Revenue_CL"]
        self.regression_methods = []
        self.classification_methods = []
        self.binary_classification_predictors = {}
        self.regression_predictors = {}

        self.binary_classifier_models = [(LogisticRegression(random_state=0, solver='lbfgs', multi_class='ovr'), "lr"),
                                         (SVC(probability=True), "svm"),
                                         (RandomForestClassifier(n_estimators=100, max_depth=4, random_state=0), "rf")]

        self.regression_models = [(LinearRegression(), "lr"),
                                  (RandomForestRegressor(n_estimators=100, max_depth=3, random_state=0), "rf"),
                                  (linear_model.RidgeCV(cv=5), "ridge"),
                                  (KNeighborsRegressor(n_neighbors=5), "knn")]

    def get_binary_classification_predictors(self):
        """
        Returns a dictionary with all the trained binary classifiers, grouped by the target y label.
        :return: Dictionary, containing all the trained, binary classification predictor performance metrics.
        """
        return self.binary_classification_predictors

    def get_regression_predictors(self):
        """
        Returns a dictionary with all the trained regressors, grouped by the target y label.
        :return: Dictionary, containing all the trained, regressor predictor performance metrics.
        """
        return self.regression_predictors

    def train_regression_models(self, dt):
        """
        Trains a set of regression models, given a specific dataframe, for direct marketing optimization.
        :param dt: Pre-processed pandas dataframe.
        """

        counts_list = [s for s in dt.columns if "Count" in s]
        acts_list = [s for s in dt.columns if "Act" in s]
        volume_list = [s for s in dt.columns if "Volume" in s]
        transactions_list = [s for s in dt.columns if "Transaction" in s]
        sales_list = [s for s in dt.columns if "Sale" in s]
        revenues_list = [s for s in dt.columns if "Revenue" in s]
        X_list = counts_list + acts_list + volume_list + transactions_list + ["Age", "Tenure", "Sex_M", "Sex_F"]

        for y_label in self.y_regression_labels:

            dt2 = dt.copy()
            dt2.drop(dt2[dt2[y_label] == 0].index, inplace=True)  # Remove all rows with zero revenue

            dt_potential = dt.copy()
            dt_potential.drop(dt_potential[dt_potential[y_label] > 0].index, inplace=True)  # Potential customers: zero revenue

            dt_train, dt_test = train_test_split(dt2, test_size=0.2)  # Train/test split

            X_train = dt_train.loc[:, dt_train.columns.isin(X_list)]
            X_test = dt_test.loc[:, dt_test.columns.isin(X_list)]
            X_potential = dt_potential.loc[:, dt_test.columns.isin(X_list)]

            y_train = dt_train[y_label]
            y_test = dt_test[y_label]

            scores_list = []
            r2_scores_list = []
            mse_list = []
            potential_list = []
            methods = []

            for model, method in self.regression_models:
                methods.append(method)
                trained_model = model.fit(X_train.values, y_train.values)
                prediction_results = trained_model.predict(X_test)
                scores_list.append(round(trained_model.score(X_test, y_test), 4))
                r2_scores_list.append(metrics.r2_score(y_test, prediction_results))
                mse_list.append(mean_squared_error(y_test, prediction_results))
                potential_list.append(trained_model.predict(X_potential))

            X_potential["Client"] = dt_potential["Client"]  # Attach back the Client ID column
            best_index, best_value = min(enumerate(mse_list), key=operator.itemgetter(1))

            self.regression_methods = methods
            self.regression_predictors[y_label] = {
                "methods": methods, "scores": scores_list,
                "r2_scores": r2_scores_list, "mse": mse_list,
                "X_potential": X_potential, "y_potential": potential_list,
                "best_method": methods[best_index]}

    def train_binary_classifier_models(self, dt):
        """
        Trains a set of binary classification models, given a specific dataframe, for direct marketing optimization.
        :param dt: Pre-processed pandas dataframe.
        """

        dt_train, dt_test = train_test_split(dt, test_size=0.2)

        counts_list = [s for s in dt.columns if "Count" in s]
        acts_list = [s for s in dt.columns if "Act" in s]
        volume_list = [s for s in dt.columns if "Volume" in s]
        transactions_list = [s for s in dt.columns if "Transaction" in s]

        X_list = counts_list + acts_list + volume_list + transactions_list + ["Age", "Tenure", "Sex_M", "Sex_F"]
        X_train = dt_train.loc[:, dt_train.columns.isin(X_list)]
        X_test = dt_test.loc[:, dt_test.columns.isin(X_list)]

        for y_label in self.y_binary_classification_labels:
            y_train = dt_train[y_label]
            y_test = dt_test[y_label].astype(int)
            fpr_list = []
            tpr_list = []
            roc_auc_list = []
            cm_list = []
            scores_list = []
            methods = []
            for model, method in self.binary_classifier_models:
                methods.append(method)
                trained_model = model.fit(X_train.values, y_train.values)
                prediction_results = trained_model.predict(X_test)
                fpr, tpr, _ = roc_curve(y_test, trained_model.predict_proba(X_test)[::, 1])
                scores_list.append(round(trained_model.score(X_test, y_test.values), 4))
                fpr_list.append(fpr)
                tpr_list.append(tpr)
                roc_auc_list.append(auc(fpr, tpr))
                cm_list.append(confusion_matrix(y_test, prediction_results))

            best_index, best_value = max(enumerate(roc_auc_list), key=operator.itemgetter(1))

            self.classification_methods = methods
            self.binary_classification_predictors[y_label] = {
                "methods": methods, "scores": scores_list,
                "fpr": fpr_list, "tpr": tpr_list, "roc_auc": roc_auc_list, "cm": cm_list,
                "best_method": methods[best_index]
            }
