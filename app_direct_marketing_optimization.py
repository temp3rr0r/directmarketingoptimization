from data_processor import DataProcessor
from plot_generator import PlotGenerator
from data_modeller import DataModeller
from flask import send_file
import dash
import dash_core_components as dcc
import dash_html_components as html
import os
from dash.dependencies import Input, Output


"""
Main web application body. Use run.sh to initiate it on an Linux machine, with "Forever" installed.
"""

external_stylesheets = ['https://codepen.io/chriddyp/pen/bWLwgP.css']  # Default css file for Dash applications
app = dash.Dash(__name__, external_stylesheets=external_stylesheets,
                suppress_callback_exceptions=True)  # Allow dynamic Tab ID allocation

with open('README.md', 'r') as about_md_text_file:
    about_md_text = about_md_text_file.read()

markdown_text = [str(about_md_text),
'''Out of the four model types:

- Linear Regression: lr
- Random Forest: rf 
- Ridge Regression: ridge 
- k-Nearest Neighbours: knn

The model with the lowest MSE was picked to generate the list of results (potential customers, sorted by the 
predicted revenue).''',
'''
Out of the 3 model types (Logistic Regression: lr, Support Vector Machines: svm, Random Forest: rf), 
the model with the best performance is picked (**largest area under the curve**).
''']


def generate_results_table(dataframe, max_rows=10):
    """
    Converts a pandas dataframe into an html table.
    :param dataframe: Pandas dataframe to show.
    :param max_rows: Maximum number of rows to show
    :return: Html table, Dash object.
    """
    return html.Table(
        [html.Tr([html.Th(col) for col in dataframe.columns])] +
        [html.Tr([
            html.Td(dataframe.iloc[i][col]) for col in dataframe.columns
        ]) for i in range(min(len(dataframe), max_rows))]
    )


data_preprocessor = DataProcessor()
plot_generator = PlotGenerator()
dt = data_preprocessor.pre_process_data()
data_modeller = DataModeller()
data_modeller.train_binary_classifier_models(dt)
data_modeller.train_regression_models(dt)

binary_classification_predictors = data_modeller.get_binary_classification_predictors()
regression_predictors = data_modeller.get_regression_predictors()

# Tab components
app.layout = html.Div([
    html.H1('Direct Marketing Optimization'),
    html.Div('by Konstantinos Theodorakos'),
    dcc.Tabs(id="tabs-example", value='tab-1-example', children=[
        dcc.Tab(label='Data', value='tab-1-example'),
        dcc.Tab(label='Data 2', value='tab-1-2-example'),
        dcc.Tab(label='Data 3', value='tab-1-3-example'),
        dcc.Tab(label='Data 4', value='tab-1-4-example'),
        dcc.Tab(label='Model', value='tab-2-example'),
        dcc.Tab(label='Results', value='tab-3-example'),
        dcc.Tab(label='About', value='tab-4-example'),
    ]),
    html.Div(id='tabs-content-example')
], style={'display': 'inline-block', 'width': '70%', 'padding-left': '15%', 'padding-right': '15%'})


@app.callback(Output('tabs-content-example', 'children'),
              [Input('tabs-example', 'value')])
def render_content(tab):
    """
    Generates dynamic Tab html content, based on the requested tab. Event listener on tab ID.
    :param tab: Tab html id.
    :return: Html Div object, recognisable by dash.
    """
    if tab == 'tab-1-example':
        return html.Div(children=[
            html.H2(children='Data Exploration'), html.Div(
                children='''The data from the excel file, were read using the Pandas python package. After 
                pre-processing and cleaning up, the dataframes are combined together, to be used by the Machine 
                Learning algorithms.'''),
            html.H4(children='Social Demographics'),
            html.Div(
                children='''Pre-processing: For the "Sex" variable, which had the string values {"M", "F"}, 
                one-hot-encoding was applied. It converted the string data type, to the integer data type (which is 
                useable by the machine learning models).'''),
            html.Div(children=[
                dcc.Graph(
                    id='graph-age-tabs',
                    figure=plot_generator.plot_box_plot("Age (years)", [s for s in dt.columns if "Age" in s], dt)
                ),
                dcc.Graph(
                    id='graph-pie_chart-tabs',
                    figure=plot_generator.plot_pie_chart(["Male", "Female"], dt)
                )
            ], style={'columnCount': 2}),
            html.Div(children=[
                html.Div(children=[
                    dcc.Graph(
                        id='graph-tenure-tabs',
                        figure=plot_generator.plot_histogram("Tenure with the bank (months)", dt.Tenure)
                    )]
                )
            ], style={'columnCount': 1})])
    elif tab == 'tab-1-2-example':
        return html.Div(children=[

            html.H4(children='Products: Account Balance'),
            html.Div(children='''Pre-processing: The variables with the prefix "ActBal" and "Count" had NaN (empty)
             values. Since they represent counts or amounts in EURO, the empty values were filled with zeros, again
             to make them use-able for the modelling process.'''),
            html.Div(children=[
                dcc.Graph(
                    id='graph-counts-tabs',
                    figure=plot_generator.plot_box_plot("Counts", [s for s in dt.columns if "Count" in s], dt)
                ),
                dcc.Graph(
                    id='graph-account_balance-tabs',
                    figure=plot_generator.plot_box_plot("Balances (EUR)", [s for s in dt.columns if "ActBal" in s], dt)
                )], style={'columnCount': 2})
        ])
    elif tab == 'tab-1-3-example':
        return html.Div(children=[
            html.H4(children='Inflow/Outflow'),
            html.Div(children=[
                dcc.Graph(
                    id='graph-VolumeCred-tabs',
                    figure=plot_generator.plot_box_plot("Monthly Credit turnover (EUR)", [s for s in dt.columns if "VolumeCred" in s], dt)
                ),
                dcc.Graph(
                    id='graph-TransactionsCred-tabs',
                    figure=plot_generator.plot_box_plot("Number of credit transactions", [s for s in dt.columns if "TransactionsCred" in s], dt)
                ),

                dcc.Graph(
                    id='graph-VolumeDeb-tabs',
                    figure=plot_generator.plot_box_plot("Monthly Turnover (EUR)", [s for s in dt.columns if "VolumeDeb" in s], dt)
                ),
                dcc.Graph(
                    id='graph-TransactionsDeb-tabs',
                    figure=plot_generator.plot_box_plot("Number of Debit transactions", [s for s in dt.columns if "TransactionsDeb" in s], dt)
                )
            ], style={'columnCount': 2}),
        ])
    elif tab == 'tab-1-4-example':
        return html.Div(children=[
            html.H4(children='Sales-Revenues'),
            html.Div(
                children='''These are the target variables.'''),
            html.Div(
                children='''The variables with the "Sale" prefix contained binary (True or False) values which are
                (YES/NO) targets in: Binary classification.'''),
            html.Div(
                children='''The variables with the "Revenue" prefix contained amounts in EUR and are used as numerical
                targets in: Regression modelling.'''),
            html.Div(children=[
                dcc.Graph(
                    id='graph-Revenue-tabs',
                    figure=plot_generator.plot_box_plot("Revenue", [s for s in dt.columns if "Revenue" in s], dt)
                )])
        ])
    elif tab == 'tab-2-example':

        y_label = "Sale_MF"
        y_label_regression = "Revenue_MF"

        methods = binary_classification_predictors[y_label]["methods"]
        cm_list = binary_classification_predictors[y_label]["cm"]

        fpr_list = binary_classification_predictors[y_label]["fpr"]
        tpr_list = binary_classification_predictors[y_label]["tpr"]
        roc_auc_list = binary_classification_predictors[y_label]["roc_auc"]

        mse_list = regression_predictors[y_label_regression]["mse"]

        return html.Div([
            html.H3('Binary Classification Models'),
            html.Div(
                children='''They can predict if a specific customer will purchase or NOT a specific service 
                (Mutual Funds, Credit Cards or Consumer Loan). For the binary classification models, all the data were 
                used. They were split to Train/Test (80%/20%).'''),
            dcc.Markdown(children=markdown_text[2]),
            html.Div([
                dcc.Dropdown(
                    id='xaxis-column',
                    options=[{'label': i, 'value': j} for i, j in
                             zip(["Mutual Funds sales", "Credit Card sales", "Consumer Loan sales"],
                                 ["Sale_MF", "Sale_CC", "Sale_CL"])],
                    value="Sale_MF"
                )
            ]),
            html.Div(id='tab-2-contents', children=[
                dcc.Graph(
                    id='graph-roc_auc-tabs',
                    figure=plot_generator.plot_receiver_operating_characteristic(methods, fpr_list, tpr_list,
                                                                                 roc_auc_list, y_label)
                ),
                dcc.Graph(
                    id='graph-confusionMatrix-rf-tabs',
                    figure=plot_generator.plot_confusion_matrix("rf", methods, y_label, cm_list)
                ),
                dcc.Graph(
                    id='graph-confusionMatrix-lr-tabs',
                    figure=plot_generator.plot_confusion_matrix("lr", methods, y_label, cm_list)
                ),
                dcc.Graph(
                    id='graph-confusionMatrix-svm-tabs',
                    figure=plot_generator.plot_confusion_matrix("svm", methods, y_label, cm_list)
                )
            ], style={'columnCount': 2}),
            html.H3('Regression Models'),
            html.Div(
                children='''The regression models are used in predicting the amount of revenue from a specific 
                customer. To train the models, only customers that had already the service were used. The data were 
                split to train/test (80%/20%).'''),
            dcc.Markdown(children=markdown_text[1]),
            html.Div([
                dcc.Dropdown(
                    id='xaxis-column-regression',
                    options=[{'label': i, 'value': j} for i, j in
                             zip(["Mutual Funds revenue", "Credit Card revenue", "Consumer Loan revenue"],
                                 ["Revenue_MF", "Revenue_CC", "Revenue_CL"])],
                    value="Revenue_MF"
                )
            ]),
            html.Div(id='tab-2-contents-regression', children=[
                dcc.Graph(
                    id='graph-bar_mse_score-tabs',
                    figure=plot_generator.plot_barchart("Mean Squared Error ({})".format(y_label_regression),
                                                        data_modeller.regression_methods, mse_list)
                )
            ], style={'columnCount': 1})
        ])
    elif tab == 'tab-3-example':

        # Delete older files, if exist
        if os.path.exists("data/dt_mf.csv"):
            os.remove("data/dt_mf.csv")
        if os.path.exists("data/dt_mf.xlsx"):
            os.remove("data/dt_mf.xlsx")
        if os.path.exists("data/dt_cc.csv"):
            os.remove("data/dt_cc.csv")
        if os.path.exists("data/dt_cc.xlsx"):
            os.remove("data/dt_cc.xlsx")
        if os.path.exists("data/dt_cl.csv"):
            os.remove("data/dt_cl.csv")
        if os.path.exists("data/dt_cl.xlsx"):
            os.remove("data/dt_cl.xlsx")

        dt_mf = regression_predictors["Revenue_MF"]["X_potential"]
        dt_mf["PredictedSales"] = regression_predictors["Revenue_MF"]["y_potential"][
            data_modeller.regression_methods.index(regression_predictors["Revenue_MF"]["best_method"])]
        dt_mf.sort_values("PredictedSales", ascending=False, inplace=True)
        dt_mf.to_csv("data/dt_mf.csv", index=None, header=True)
        dt_mf.to_excel("data/dt_mf.xlsx")

        dt_cc = regression_predictors["Revenue_CC"]["X_potential"]
        dt_cc["PredictedSales"] = regression_predictors["Revenue_CC"]["y_potential"][
            data_modeller.regression_methods.index(regression_predictors["Revenue_CC"]["best_method"])]
        dt_cc.sort_values("PredictedSales", ascending=False, inplace=True)
        dt_cc.to_csv("data/dt_cc.csv", index=None, header=True)
        dt_cc.to_excel("data/dt_cc.xlsx")

        dt_cl = regression_predictors["Revenue_CL"]["X_potential"]
        dt_cl["PredictedSales"] = regression_predictors["Revenue_CL"]["y_potential"][
            data_modeller.regression_methods.index(regression_predictors["Revenue_CL"]["best_method"])]
        dt_cl.sort_values("PredictedSales", ascending=False, inplace=True)
        dt_cl.to_csv("data/dt_cl.csv", index=None, header=True)
        dt_cl.to_excel("data/dt_cl.xlsx")

        return html.Div([
            dcc.Markdown(children='''The regression model with the **lowest MSE** was picked to generate the lists. 
            The lists are sorted in descending order (higher to lower), 
                by the variable **Predicted Sales**. You can download the lists in both CSV and Microsoft 
                Excel format.'''),
            html.H4(children='Potential Customers: Revenue Mutual Fund'),
            generate_results_table(dt_mf[["Client", "Age", "Tenure", "ActBal_CA", "ActBal_SA", "PredictedSales"]]),
            html.Div(children=[html.A("Download CSV", href="/download_mutual_funds/")]),
            html.Div(children=[html.A("Download Excel", href="/download_mutual_funds_excel/")]),
            html.H4(children='Potential Customers: Revenue Credit Cards'),
            generate_results_table(dt_cc[["Client", "Age", "Tenure", "ActBal_CA", "ActBal_SA", "PredictedSales"]]),
            html.Div(children=[html.A("Download CSV", href="/download_credit_cards/")]),
            html.Div(children=[html.A("Download Excel", href="/download_credit_cards_excel/")]),
            html.H4(children='Potential Customers: Revenue Consumer Loan'),
            generate_results_table(dt_cl[["Client", "Age", "Tenure", "ActBal_CA", "ActBal_SA", "PredictedSales"]]),
            html.Div(children=[html.A("Download CSV", href="/download_consumer_loan/")]),
            html.Div(children=[html.A("Download Excel", href="/download_consumer_loan_excel/")]),
        ])
    elif tab == 'tab-4-example':
        return html.Div([
            dcc.Markdown(children=markdown_text[0]),
        ])


@app.server.route('/download_mutual_funds/')
def download_mutual_funds():
    """
    Flask based route, in order to serve csv file downloads.
    :return: A CSV file for mutual funds.
    """
    return send_file('data/dt_mf.csv', mimetype='text/csv', attachment_filename='dt_mf.csv',
                     as_attachment=True)


@app.server.route('/download_mutual_funds_excel/')
def download_mutual_funds_excel():
    """
    Flask based route, in order to serve excel file downloads.
    :return: An XLSX file for mutual funds.
    """
    return send_file('data/dt_mf.xlsx', mimetype='text/xls', attachment_filename='dt_mf.xlsx',
                     as_attachment=True)


@app.server.route('/download_credit_cards/')
def download_credit_cards():
    """
    Flask based route, in order to serve csv file downloads.
    :return: A CSV file for credit cards.
    """
    return send_file('data/dt_cc.csv', mimetype='text/csv', attachment_filename='dt_cc.csv',
                     as_attachment=True)


@app.server.route('/download_credit_cards_excel/')
def download_credit_cards_excel():
    """
    Flask based route, in order to serve excel file downloads.
    :return: An XLSX file for credit cards.
    """
    return send_file('data/dt_cc.xlsx', mimetype='text/xls', attachment_filename='dt_cc.xlsx',
                     as_attachment=True)


@app.server.route('/download_consumer_loan/')
def download_consumer_loan():
    """
    Flask based route, in order to serve csv file downloads.
    :return: A CSV file for consumer loans.
    """
    return send_file('data/dt_cl.csv', mimetype='text/csv', attachment_filename='dt_cl.csv',
                     as_attachment=True)


@app.server.route('/download_consumer_loan_excel/')
def download_consumer_loan_excel():
    """
    Flask based route, in order to serve excel file downloads.
    :return: An XLSX file for consumer loans.
    """
    return send_file('data/dt_cl.xlsx', mimetype='text/xls', attachment_filename='dt_cl.xlsx',
                     as_attachment=True)


@app.callback(
    Output('graph-bar_mse_score-tabs', 'figure'),
    [Input('xaxis-column-regression', 'value')])
def update_barchart(y_label_regression):
    """
    Event listener that updates the barchart plot, given a specific y-target variable.
    :param y_label_regression: The y-target variable string.
    :return: A dash figure barchart object.
    """
    return plot_generator.plot_barchart(
        "Mean Squared Error ({})".format(y_label_regression), data_modeller.regression_methods,
        regression_predictors[y_label_regression]["mse"])


@app.callback(
    Output('graph-roc_auc-tabs', 'figure'),
    [Input('xaxis-column', 'value')])
def update_receiver_operator_characteristic(y_label):
    """
    Event listener that updates the ROC plot, given a specific y-target variable.
    :param y_label: The y-target variable string.
    :return: A dash figure barchart object.
    """
    return plot_generator.plot_receiver_operating_characteristic(
        binary_classification_predictors[y_label]["methods"], binary_classification_predictors[y_label]["fpr"],
        binary_classification_predictors[y_label]["tpr"], binary_classification_predictors[y_label]["roc_auc"], y_label)


@app.callback(
    Output('graph-confusionMatrix-rf-tabs', 'figure'),
    [Input('xaxis-column', 'value')])
def update_confusion_matrix_rf(y_label):
    """
    Event listener that updates the confusion matrix plot, given a specific y-target variable, for Random Forest
    classification.
    :param y_label: The y-target variable string.
    :return: A dash figure Confusion Matrix object.
    """
    return plot_generator.plot_confusion_matrix("rf", binary_classification_predictors[y_label]["methods"],
                                                y_label, binary_classification_predictors[y_label]["cm"])


@app.callback(
    Output('graph-confusionMatrix-lr-tabs', 'figure'),
    [Input('xaxis-column', 'value')])
def update_confusion_matrix_lr(y_label):
    """
    Event listener that updates the confusion matrix plot, given a specific y-target variable, for Logistic Regression.
    :param y_label: The y-target variable string.
    :return: A dash figure Confusion Matrix object.
    """
    return plot_generator.plot_confusion_matrix("lr", binary_classification_predictors[y_label]["methods"],
                                                y_label, binary_classification_predictors[y_label]["cm"])


@app.callback(
    Output('graph-confusionMatrix-svm-tabs', 'figure'),
    [Input('xaxis-column', 'value')])
def update_confusion_matrix_svm(y_label):
    """
    Event listener that updates the confusion matrix plot, given a specific y-target variable, for Support Vector
    Machines classification.
    :param y_label: The y-target variable string.
    :return: A dash figure Confusion Matrix object.
    """
    return plot_generator.plot_confusion_matrix("svm", binary_classification_predictors[y_label]["methods"],
                                                y_label, binary_classification_predictors[y_label]["cm"])


if __name__ == '__main__':
    # app.run_server(debug=False)  # Uncomment for debug mode.
    app.run_server(host='0.0.0.0')
