## Task:

Create a generic solution to approach clients in a direct marketing campaign for banking service products for any of three products (consumer loans, mutual funds, credit cards). The solution should be easily extendable with limited modification and applicable on also other products (assuming the data is provided in the same structure). Use the provided dummy data to select the best 100 clients. The list of clients should be ranked according to the propensity to buy.

### Technical Report

The website was implemented in Python. **Dash** was mainly used to provide the web user interface.

Python packages used:

- **Dash**: Web UI.
- **Plotly**: Plots and figures.
- **Flask**: Routing backend.
- **Pandas**: Data pre-processing and manipulation.
- **Sci-kit Learn**: Machine Learning algorithms.
- **Jupyter**: For testing scripts, data manipulation and ML algorithms.
- **Integrated Development Environment (IDE)**: JetBrains Pycharm 2018.2.4

##### Data flow diagram:

![alt text](http://epidemicsimulator.com/images/DirectMarketingOptimizationDataFlow.svg "Data flow Diagram")

##### Class diagram:

![alt text](http://epidemicsimulator.com/images/class_diagram.png "Class Diagram")

##### Testing:

A mix of unit and regression tests is added, for the classes ```DataModeller``` and ```DataProcessor```:

![alt text](http://epidemicsimulator.com/images/unit_regression_tests.png "Unit and Regression tests")

Regarding coverage, the class ```PlotGenerator``` and the main script ```app_direct_marketing_optimization.py``` that generate UI elements, were intentionally not covered. However ```DataModeller``` and ```DataProcessor``` classes were covered significantly:

![alt text](http://epidemicsimulator.com/images/test_coverage.png "Coverage") 

### Deployment Approach

##### Installation & Deployment

The project repository is located at: [https://bitbucket.org/temp3rr0r/directmarketingoptimization](https://bitbucket.org/temp3rr0r/directmarketingoptimization). You can clone it with:
```
git clone https://bitbucket.org/temp3rr0r/directmarketingoptimization
```

To install the python packages, run:
```
pip install --upgrade pip
pip install numpy pandas sklearn dash plotly flask openpyxl -U
```

To run the web app directly:
```
python app_direct_marketing_optimization.py
```

To run the web app on a linux server as a service, with Forever installed (see: [https://www.npmjs.com/package/forever](https://www.npmjs.com/package/forever)), run:
```
chmod +x run.sh
./run.sh
```
or
```
forever start -c python -l /tmp/forever.log -o /tmp/out.log -e /tmp/error.log app_direct_marketing_optimization.py
```

##### Scaling

The main website application is the script ```app_direct_marketing_optimization.py```, which can be run on a Linux-based server, after cloning the repository. Ideally, the whole website could be inside a docker container, however there wasn't enough time to support this option.
The debug switch should be set to false (File: ```app_direct_marketing_optimization.py``` Line: 505).

The "Forever" application can manage the automatic start-up/restarting of the web app as a service and the error logging.

For large scale deployment, the ```DataProcessor``` and ```DataModeller``` classes, could be put into separate Flask RESTful API services (as **Pre-Processing** and **Machine Learning** services). This way, the load for gathering, processing the data and training the models will be split. These services could even be put into horizontal or vertically scalable (hybrid cloud) containers.

The services could be invoked periodically (i.e. every hour). They could grab data from a DBMS system like Apache Hadoop or a SQL/No-SQL variant. Then with the new data, several new ML models could be trained and stored to disk as serialised objects. If any of the new models outperform the pre-existing, they could replace them on the fly for the real-time inferences.

For the Pre-Processing service, it is advised to use a cloud-based Virtual Machine (or bare-metal) with large amounts of RAM and high Input/output network & storage operations per second.

The Machine Learning service should be deployed onto a cloud Virtual machine (or bare-metal) with a large count of physical CPU cores and lots of RAM.