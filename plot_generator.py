import plotly.graph_objs as go
import pandas as pd


class PlotGenerator:
    """
    Generates and returns plotly figures, given data and title.
    """

    @staticmethod
    def plot_pie_chart(labels, dt):
        """
        Returns a plotly pie-chart figure object (for male/female counts).
        :param labels: List of strings, labels for the pie chart categories.
        :param dt: Pandas data-set.
        :return: A figure plotly object, containing a pie-chart.
        """
        m, f = dt["Sex_M"].values.sum(), dt["Sex_F"].values.sum()
        values = [f, m]
        trace = go.Pie(labels=labels, values=values)
        fig = go.Figure(data=[trace])
        return fig

    @staticmethod
    def plot_box_plot(title, x_data, dt):
        """
        Returns a plotly box-plot figure object.
        :param title: Figure title.
        :param x_data: List of the columns to show.
        :param dt: The full pandas dataframe.
        :return: A figure plotly object, containing a box-plot.
        """
        y_data = []
        colors = []
        color_palette = ['rgba(93, 164, 214, 0.5)', 'rgba(255, 144, 14, 0.5)', 'rgba(44, 160, 101, 0.5)',
                         'rgba(255, 65, 54, 0.5)', 'rgba(207, 114, 255, 0.5)', 'rgba(127, 96, 0, 0.5)']
        for idx, column in enumerate(x_data):
            if column not in ["Client", "Sex"]:
                y_data.append(dt[column])
                colors.append(color_palette[idx % len(color_palette)])

        traces = []

        for xd, yd, cls in zip(x_data, y_data, colors):
            traces.append(go.Box(
                y=yd,
                name=xd,
                # boxpoints='all',
                boxpoints='suspectedoutliers',
                jitter=0.5,
                whiskerwidth=0.2,
                fillcolor=cls,
                marker=dict(
                    size=2,
                ),
                line=dict(width=1),
            ))

        layout = go.Layout(
            title=title,
            yaxis=dict(
                autorange=True,
                showgrid=True,
                zeroline=True,
                dtick=5,
                gridcolor='rgb(255, 255, 255)',
                gridwidth=1,
                zerolinecolor='rgb(255, 255, 255)',
                zerolinewidth=2,
            ),
            margin=dict(
                l=40,
                r=30,
                b=80,
                t=100,
            ),
            paper_bgcolor='rgb(243, 243, 243)',
            plot_bgcolor='rgb(243, 243, 243)',
            showlegend=False
        )

        fig = go.Figure(data=traces, layout=layout)
        return fig

    @staticmethod
    def plot_receiver_operating_characteristic(methods, fpr_list, tpr_list, roc_auc_list, y_label):
        """
        Returns a plotly Receiver Operating Characteristic (ROC) figure object.
        :param methods: List of machine learning method abbreviations.
        :param fpr_list: False positives list. Results from Sci-kit learn's "roc_curve" operation.
        :param tpr_list: True positives list. Result from Sci-kit learn's "roc_curve" operation.
        :param roc_auc_list: Area under the curve list. Result from Sci-kit learn's "auc" operation.
        :param y_label: Y target variable.
        :return: A figure plotly object, containing an ROC plot.
        """
        lw = 2
        traces = []
        for idx, method in enumerate(methods):
            roc_auc = roc_auc_list[idx]
            traces.append(go.Scatter(x=fpr_list[idx], y=tpr_list[idx], mode='lines',
                                     name='{} ROC curve (area = %0.2f)'.format(method) % roc_auc))

        traces.append(go.Scatter(x=[0, 1], y=[0, 1], mode='lines',
                                 line=dict(color='navy', width=lw, dash='dash'),
                                 showlegend=False))

        layout = go.Layout(title='Receiver operating characteristic: {}'.format(y_label),
                           xaxis=dict(title='False Positive Rate'),
                           yaxis=dict(title='True Positive Rate'),
                           legend={"x": 1, "y": 0})

        fig = go.Figure(data=traces, layout=layout)
        return fig

    @staticmethod
    def plot_confusion_matrix(method, methods, y_label, cm_list):
        """
        Returns a plotly Confusion Matrix (from heatmap) figure object.
        :param method: Abbreviation of the machine learning method: lr, svm, rf.
        :param methods: List of all the possible ML methods.
        :param y_label: Y target variable.
        :param cm_list: Confusion Matrix list. Result from Sci-kit learn's "confusion_matrix" operation.
        :return: A figure plotly object, containing a Confusion Matrix plot.
        """
        idx = methods.index(method)
        trace = go.Heatmap(z=cm_list[idx], x=["True", "False"], y=["True", "False"], colorscale="Jet")
        layout = go.Layout(title='Confusion Matrix: {} (Model: {})'.format(y_label, methods[idx]),
                           xaxis=dict(title='Predicted Label'),
                           yaxis=dict(title='True Label'))
        fig = go.Figure([trace], layout=layout)
        return fig

    @staticmethod
    def plot_histogram(title, x_data):
        """
        Returns a plotly Histogram figure object.
        :param title: Part of the plot's title.
        :param x_data: Data to show in the histogram (pandas column).
        :return: A figure plotly object, containing a Histogram plot.
        """
        trace = go.Histogram(x=x_data)
        layout = go.Layout(title='Histogram: {}'.format(title))
        fig = go.Figure([trace], layout=layout)
        return fig

    @staticmethod
    def plot_barchart(title, x_data, y_data):
        """
        Returns a plotly Bar chart figure object.
        :param title: Part of the plot's title.
        :param x_data: X-axis list of legends.
        :param y_data: Data to show in the histogram (pandas column).
        :return: A figure plotly object, containing a Bar chart plot.
        """
        trace = go.Bar(x=x_data, y=y_data)
        layout = go.Layout(title='Bar chart: {}'.format(title))
        fig = go.Figure([trace], layout=layout)
        return fig
