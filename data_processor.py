import pandas as pd


class DataProcessor:
    """
    Converts excel files into a pre-processed (NAs, fill, inner-join etc) pandas dataframe.
    """
    @staticmethod
    def pre_process_data(filename="data/Task_ML_Engineer_Dataset.xlsx"):
        """
        Returns a pre-processed excel file, given a specific Microsoft Excel (*.xlsx) file with the sheets:
        Soc_Dem, Products_ActBalance, Inflow_Outflow and Sales_Revenues. It fills NaNs with zeros on a specific sheet
        and joins the sheet data on a specific client ID.
        :return: Pre-processed pandas dataframe.
        """

        try:
            # Read data
            dt_soc_dem = pd.read_excel(filename, sheet_name="Soc_Dem")
            dt_soc_dem = pd.get_dummies(dt_soc_dem, prefix=["Sex"])  # One-hot encoding

            dt_products_act_balance = pd.read_excel(filename, sheet_name="Products_ActBalance")
            dt_products_act_balance = dt_products_act_balance.fillna(0)  # Fill empty values with zeros
            dt_inflow_outflow = pd.read_excel(filename, sheet_name="Inflow_Outflow")
            dt_sales_revenues = pd.read_excel(filename, sheet_name="Sales_Revenues")

            # Inner joins
            dt = pd.merge(dt_soc_dem, dt_sales_revenues, on="Client", how="inner")
            dt = pd.merge(dt, dt_products_act_balance, on="Client", how="inner")
            dt = pd.merge(dt, dt_inflow_outflow, on="Client", how="inner")

            dt = dt.dropna()  # Remove rows with nans
        except FileNotFoundError as fnf:
            print("FileNotFoundError Exception: {}".format(fnf))
        except TypeError as te:
            print("TypeError exception: {}".format(te))
        except NameError as ne:
            print("NameError exception: {}".format(ne))
        except Exception as ex:
            print("Generic exception: {}".format(ex))

        return dt
