import pytest
import pandas
import numpy
from data_processor import DataProcessor


@pytest.fixture
def data_processor():
    """
    Instantiates a DataProcessor.
    :return: A DataProcessor object.
    """
    return DataProcessor()


def test_pre_process_data_no_file(data_processor):
    """
    Checks if pre_process_data catches, no file exception, given invalid file path.
    :param data_processor: A DataProcessor object.
    """
    with pytest.raises(UnboundLocalError):
        data_processor.pre_process_data("no_file")


@pytest.mark.parametrize("file_path", [
    (["test_files/small_dataset.xlsx"]),
    (["test_files/large_dataset.xlsx"])
])
def test_pre_process_data_valid_file(data_processor, file_path):
    """
    Checks if pre_process_data can properly process a valid excel file.
    :param data_processor: A DataProcessor object.
    """
    dt = data_processor.pre_process_data("test_files/small_dataset.xlsx")
    assert dt is not None
    assert type(dt) not in [list, dict, tuple, bytearray, set, object, staticmethod, numpy.ndarray]
    assert type(dt) is pandas.core.frame.DataFrame



def test_pre_process_data_invalid_file(data_processor):
    """
    Checks if pre_process_data catches reading exception, given an invalid or corrupted excel file.
    :param data_processor: A DataProcessor object.
    """
    with pytest.raises(Exception):
        data_processor.pre_process_data("test_files/corrupted_file.xlsx")
