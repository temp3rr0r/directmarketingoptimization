import pytest
from data_modeller import DataModeller
from data_processor import DataProcessor


@pytest.fixture
def data_processor():
    """
    Instantiates a DataProcessor.
    :return: A DataProcessor object.
    """
    return DataProcessor()


@pytest.fixture
def data_modeller():
    """
    Instantiates a DataModeller.
    :return: A DataModeller object.
    """
    return DataModeller()


def test_initialize(data_modeller):
    """
    Checks if DataModeller can properly initialize.
    :param data_modeller: A DataModeller object.
    """
    assert data_modeller.regression_methods == []
    assert data_modeller.classification_methods == []
    assert data_modeller.y_regression_labels == ["Revenue_MF", "Revenue_CC", "Revenue_CL"]
    assert data_modeller.y_binary_classification_labels == ["Sale_MF", "Sale_CC", "Sale_CL"]
    assert data_modeller.binary_classification_predictors == {}
    assert data_modeller.regression_predictors == {}


def test_get_binary_classification_predictors_empty(data_modeller):
    """
    Checks if DataModeller returns empty classification predictors if just initialized.
    :param data_modeller: A DataModeller object.
    """
    assert data_modeller.get_binary_classification_predictors() == {}


def test_get_regression_predictors_empty(data_modeller):
    """
    Checks if DataModeller returns empty regression predictors if just initialized.
    :param data_modeller: A DataModeller object.
    """
    assert data_modeller.get_regression_predictors() == {}


def test_train_regression_models_not_enough_samples(data_modeller, data_processor):
    """
    Checks if properly throws exception when trying to train regression models with zero samples.
    :param data_modeller: A DataModeller object.
    :param data_processor: A DataProcessor object.
    """
    with pytest.raises(ValueError):
        dt = data_processor.pre_process_data("test_files/small_dataset.xlsx")
        data_modeller.train_regression_models(dt)


def test_train_binary_classifier_models_not_enough_samples(data_modeller, data_processor):
    """
    Checks if properly throws exception when trying to train classifier models with zero samples.
    :param data_modeller: A DataModeller object.
    :param data_processor: A DataProcessor object.
    """
    with pytest.raises(ValueError):
        dt = data_processor.pre_process_data("test_files/small_dataset.xlsx")
        data_modeller.train_binary_classifier_models(dt)


@pytest.mark.parametrize("file_path", [("test_files/large_dataset.xlsx")])
def test_train_binary_classifier_models(data_modeller, data_processor, file_path):
    """

    :param data_modeller: A DataModeller object.
    :param data_processor: A DataProcessor object.
    """
    dt = data_processor.pre_process_data(file_path)
    data_modeller.train_binary_classifier_models(dt)
    binary_classifiers = data_modeller.get_binary_classification_predictors()
    assert binary_classifiers is not None
    assert type(binary_classifiers) is dict
    assert binary_classifiers.keys() is not None
    assert binary_classifiers["Sale_MF"] is not None
    assert binary_classifiers["Sale_MF"]["roc_auc"] is not None
    assert type(binary_classifiers["Sale_MF"]["roc_auc"]) is list
    assert binary_classifiers["Sale_MF"]["tpr"][0][0] >= 0.0


@pytest.mark.parametrize("file_path", [("test_files/large_dataset.xlsx")])
def test_train_regression_models(data_modeller, data_processor, file_path):
    """

    :param data_modeller: A DataModeller object.
    :param data_processor: A DataProcessor object.
    """
    dt = data_processor.pre_process_data(file_path)
    data_modeller.train_regression_models(dt)
    regressors = data_modeller.get_regression_predictors()
    assert regressors is not None
    assert type(regressors) is dict
    assert regressors.keys() is not None
    assert regressors["Revenue_CL"] is not None
    assert regressors["Revenue_CL"]["r2_scores"] is not None
    assert type(regressors["Revenue_CL"]["mse"]) is list
    assert regressors["Revenue_CL"]["mse"][0] >= 0.0
